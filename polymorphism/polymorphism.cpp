// polymorphism.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>
#include <memory>
#include "Car.h"

std::shared_ptr<Car> manufacture(const std::string& brand) {
    if (brand.compare("Tesla") == 0) {
        return std::shared_ptr<Tesla> (new Tesla);
    }
    else if (brand.compare("Porsche") == 0) {
        return std::shared_ptr<Porsche>(new Porsche);
    }
    else {
        return std::shared_ptr<Car>(new Car);
    }
    return nullptr;
}

int main()
{
    std::shared_ptr<Car> car1 = manufacture("Tesla");         // return pointer to Tesla object
    car1->start();                            // print "Safe driving!"

    std::shared_ptr<Car> car2 = manufacture("Porsche");       // return pointer to Porsche object
    car2->start();                            // print "Nice car!"

    std::shared_ptr<Car> car3 = manufacture("fck");           // return pointer to Car object for any other strings
    car3->start();
}


