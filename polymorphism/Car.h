#pragma once

class Car {
public:
	Car() = default;
	virtual void start();
	virtual ~Car() = default;
};

class Tesla:public Car {
public:
	Tesla() = default;
	virtual void start() override;
	virtual ~Tesla() = default;
};

class Porsche :public Car {
public:
	Porsche() = default;
	virtual void start() override;
	virtual ~Porsche() = default;
};